# apt 相关操作

## 查看已经安装的软件包

```bash
apt list --installed
```

# 安装 docker-ce

```bash
# 安装 https 包
sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common

# 安装 key
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"

sudo apt-get install docker-ce docker-ce-cli containerd.io
# 查看包
apt-cache madison docker-ce
# 安装对应的版本
sudo apt-get install docker-ce=<VERSION_STRING> docker-ce-cli=<VERSION_STRING> containerd.io
# 把用户加入到 docker组 
  sudo usermod -aG docker jingmin
```

# docker 安装 redis

```bash
docker search  redis

docker pull  redis:3.2

docker run -p 6379:6379 -v $PWD/data:/data  -d redis:latest redis-server --appendonly yes

docker run -p 6379:6379 -v ~/docker/redis-data:/data  -d redis:latest redis-server --appendonly yes


docker run -p 6379:6379 -v ~/docker/redis-data:/data --restart always --name=redis-server -d redis:latest redis-server

docker exec -ti edis-server redis-cli

# 杀死所有正在运行的容器
docker kill $(docker ps -a -q)
# 删除所有已经停止的容器
docker rm $(docker ps -a -q)
# 强制删除镜像名称中包含“doss-api”的镜像
docker rmi --force $(docker images | grep doss-api | awk '{print $3}')
# 删除所有未使用数据
docker system prune
# 只删除未使用的volumes
docker volume prune

# 如果容器已经被创建，我们想要修改容器的重启策略
docker update --restart no mynginx

```

## docker 启动参数

在容器退出或断电开机后，docker可以通过在容器创建时的--restart参数来指定重启策略；
- no  不自动重启容器. (默认值)
- on-failure  容器发生error而退出(容器退出状态不为0)重启容器,可以指定重启的最大次数，如：on-failure:10
- unless-stopped  在容器已经stop掉或Docker stoped/restarted的时候才重启容器
- always  在容器已经stop掉或Docker stoped/restarted的时候才重启容器，手动stop的不算

## git

```bash
# 设置不分页
git config --global core.pager tig
# 设置不分页
git config --global core.pager "less -FRSX"

# 清除存储
git stash clear

# 删除一个存储
git stash drop stash@{0}

# 合并某个commit
git cherry-pick

```
