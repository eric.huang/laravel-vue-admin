# laravel

## composer 管理

### composer 国内源

```bash
composer config -g repo.packagist composer https://packagist.laravel-china.org

composer config repo.packagist composer https://packagist.laravel-china.org

composer config -g --unset repos.packagist
```

### composer 设置代理

```bash
export https_proxy='127.0.0.1:8888'
export http_proxy='127.0.0.1:8888'
composer require tymon/jwt-auth:^1.0
```

## 安装

### jwt 安装

- [官网文档](https://jwt-auth.readthedocs.io/en/develop/laravel-installation/)
- [GitHub](https://github.com/tymondesigns/jwt-auth)
- [JWT 超详细分析](https://learnku.com/articles/17883#6bbce7)
- [JWT 完整使用详解](https://learnku.com/articles/10885/full-use-of-jwt)

```bash
composer require tymon/jwt-auth:dev-develop --prefer-source
composer require tymon/jwt-auth:^1.0
php artisan vendor:publish --provider="Tymon\JWTAuth\Providers\LaravelServiceProvider"
artisan jwt:secret
```

## 图片处理

imagemagick 是php中和gd一样的一个处理图形的库

- [image.intervention.io 官网](http://image.intervention.io/getting_started/introduction)
- [CSDN laravel 使用Intervention/image生成缩略图](https://blog.csdn.net/lingchen__/article/details/78193205)

```bash
composer require intervention/image

php artisan vendor:publish --provider="Intervention\Image\ImageServiceProviderLaravel5"
```

## 进坑指南

## artisan 常用命令

```bash
# 在加入配置等之后，要使用优化命令
php artisan optimize
php artisan clear-compiled
```

## 数据库相关操作

### 生成迁移文件

加入数据迁移之后 使用 **composer dumpautoload** 才能生效

```bash
php artison make:migration create_role_table --create=role
```

### 建立数据库及设置权限

```sql
create database homestead default charset utf8mb4;
grant all privileges on `homestead`.* to homestead@'%' identified by 'secret';
flush privileges;
```

```bash
php artisan migrate
```

## 权限管理系统

参照系统

- [zizaco/entrust](https://github.com/Zizaco/entrust)
- [laravel-permission](https://github.com/spatie/laravel-permission)
- [bouncer](https://github.com/JosephSilber/bouncer)

- [[ Laravel 5.5 文档 ] 安全系列 —— 在 Laravel 中实现对资源操作的权限控制](https://laravelacademy.org/post/8313.html)

## queue

## 安装 Supervisor

```bash
sudo apt-get install supervisor
```

## 常用库

### 文件管理

- [https://flysystem.thephpleague.com/docs/](https://flysystem.thephpleague.com/docs/)
