# 环境配置

```bash
sudo npm config set --global phantomjs_cdnurl=http://cnpmjs.org/downloads

sudo npm config set --global sass_binary_site=https://npm.taobao.org/mirrors/node-sass/

sudo npm config set --global registry=https://registry.npm.taobao.org

sudo npm config set --global disturl=https://npm.taobao.org/dist
```

```bash

# 安装 vue 环境

```shell
npm install @types/axios @types/js-cookie @types/lodash @types/node js-cookie lodash typescript ts-loader axios --save-dev

npm install sass sass-loader --save-dev

npm install vue-class-component vue-router vuex --save-dev
npm install vuex-module-decorators --save-dev
```
# Element

- [官网](https://element.eleme.cn/#/zh-CN)

# Vue Property Decorator

## 参照网站

- [官网](https://github.com/kaorun343/vue-property-decorator)
- [简书手册](https://www.jianshu.com/p/f454eabf9e72)
- [vue-property-decorator使用指南 掘金](https://juejin.im/post/5c173a84f265da610e7ffe44)

# vuex-module-decorators

## 参照网站

- [官网](https://championswimmer.in/vuex-module-decorators/pages/installation.html)
- [vuex-module-decorators 使用记录 typescript vuex](https://www.jianshu.com/p/aa570c01ce46)

```bash
npm install vuex-module-decorators --save-dev  
```

# Vuex

## Action 与 mutation 的区别

```js
const store = new Vuex.Store({
    state: {
        count: 0
    },
    mutations: {
        // 加1
        INCREMENT(state) {
            state.count++;
        },
        // 减1
        DECREMENT(state) {
            state.count--
        }
    },
    actions: {
        increment(context) {
            context.commit("INCREMENT");
        },
        decrement(context) {
            context.commit("DECREMENT");
        }
    }
})
```

- action 可以做异步处理, mutation做同步处理
- 在vue 中， 只有mutation 才能改变state.  mutation 类似事件， 每一个mutation都有一个类型和一个处理函数， 因为只有mutation 才能改变state, 所以处理函数自动会获得一个默认参数 state. 所谓的类型其实就是名字， action去comit 一个mutation, 它要指定去commit哪个mutation, 所以mutation至少需要一个名字， commit mutation 之后， 要做什么事情， 那就需要给它指定一个处理函数， 类型(名字) + 处理函数就构成了mutation. 现在store.js添加mutation.
- Vue 建议我们mutation 类型用大写常量表示， 修改一下， 把mutation 类型改为大写
- action去commit mutations, 所以还要定义action. store.js 里面添加actions.

```javascript
import {
    mapActions
} from "vuex";

export default {
    methods: {
        ...mapActions(["increment", "decrement"])
    }
}
```

# vuex-class

用于导出使用Vuex

# tsconfig.json 配置

```json
{
  "include": [
    "src/**/*"
  ],
  "exclude": [
    "node_modules"
  ],
  "compilerOptions": {
    "baseUrl": ".",
    "paths": {
      "@/*": ["*", "src/*"]
    },
    "jsx": "preserve",
    "jsxFactory": "h",
    "allowSyntheticDefaultImports": true,
    "experimentalDecorators": true,
    "allowJs": true,
    "module": "esnext",
    "target": "es5",
    "moduleResolution": "node",
    "isolatedModules": true,
    "lib": [
      "dom",
      "es5",
      "es6",
      "es7",
      "es2015.promise"
    ],
    "sourceMap": true,
    "pretty": true
  }
}

添加并配置 tslint.json
Tip：同上，文件放置于根目录，配置内容如下：
{
  "extends": [
    "tslint-eslint-rules"
  ],
  "rulesDirectory": [],
  "rules": {
    "adjacent-overload-signatures": false,  //  Enforces function overloads to be consecutive.
    "ban-comma-operator": true, //禁止逗号运算符。
    "ban-type": [true, ["object","User {} instead."],["string"]], //禁止类型
    "no-any": false,//不需使用any类型
    "no-empty-interface":true, //禁止空接口 {}
    "no-internal-module": true, //不允许内部模块
    "no-magic-numbers": false, //不允许在变量赋值之外使用常量数值。当没有指定允许值列表时，默认允许-1,0和1
    "no-namespace": [ true,"allpw-declarations"], //不允许使用内部modules和命名空间
    "no-non-null-assertion": true , //不允许使用!后缀操作符的非空断言。
    "no-parameter-reassignment": true, //不允许重新分配参数
    "no-reference": true, // 禁止使用/// <reference path=> 导入 ，使用import代替
    "no-unnecessary-type-assertion": false, //如果类型断言没有改变表达式的类型就发出警告
    "no-var-requires": false, //不允许使用var module = require("module"),用 import foo = require('foo')导入
    "prefer-for-of":true,  //建议使用for(..of)
    "promise-function-async": false, //要求异步函数返回promise
    "typedef": [
        true, 
        {
        "call-signature": "nospace",
        "index-signature": "nospace",
        "parameter": "nospace",
        "property-declaration": "nospace",
        "variable-declaration": "nospace"
      }
    ], //需要定义的类型存在
    "typedef-whitespace": true, //类型声明的冒号之前是否需要空格
    "unified-signatures": true, //重载可以被统一联合成一个
        //function 专用
    "await-promise": false,  //警告不是一个promise的await
    "ban": [
      true,
      "eval",
      {"name": "$", "message": "please don't"},
      ["describe", "only"],
      {"name": ["it", "only"], "message": "don't focus tests"},
      {
        "name": ["chai", "assert", "equal"],
        "message": "Use 'strictEqual' instead."
      },
      {"name": ["*", "forEach"], "message": "Use a regular for loop instead."}
        ],
    "curly": true, //for if do while 要有括号
    "forin":true, //用for in 必须用if进行过滤
    "import-blacklist":true, //允许使用import require导入具体的模块
    "label-postion": true, //允许在do/for/while/swith中使用label
    "no-arg":true, //不允许使用 argument.callee
    "no-bitwise":true, //不允许使用按位运算符
    "no-conditional-assignmen": true, //不允许在do-while/for/if/while判断语句中使用赋值语句
    "no-console": false, //不能使用console
    "no-construct": true, //不允许使用 String/Number/Boolean的构造函数
    "no-debugger": true, //不允许使用debugger
    "no-duplicate-super": true, //构造函数两次用super会发出警告
    "no-empty":true, //不允许空的块
    "no-eval": true, //不允许使用eval
    "no-floating-promises": false, //必须正确处理promise的返回函数
    "no-for-in-array": false, //不允许使用for in 遍历数组
    "no-implicit-dependencies": false, //不允许在项目的package.json中导入未列为依赖项的模块
    "no-inferred-empty-object-type": false, //不允许在函数和构造函数中使用{}的类型推断
    "no-invalid-template-strings":  true, //警告在非模板字符中使用${
    "no-invalid-this": true, //不允许在非class中使用 this关键字
    "no-misused-new": true, //禁止定义构造函数或new class
    "no-null-keyword": false, //不允许使用null关键字
    "no-object-literal-type-assertion": false, //禁止object出现在类型断言表达式中
    "no-return-await": true, //不允许return await
    "arrow-parens":  false //箭头函数定义的参数需要括号
  },
  "ecmaFeatures": {
      "objectLiteralShorthandProperties": true // 对象字面量属性名简写
  }
}
```

