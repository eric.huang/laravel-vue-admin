# PHP

## 问题

- [x] instanceof Closure

## php中反射类处理

- [官方文档](https://php.net/manual/zh/book.reflection.php)

## 常用函数

### compact 函数 把变量通过变量名转成数组

```php
<?php
$firstname = "Peter";
$lastname = "Griffin";
$age = "41";

$result = compact("firstname", "lastname", "age");

print_r($result);
?>
```

## 配置xdebug

### 参照资料

- [xdebug开发调试PHP](https://cloud.tencent.com/developer/article/1335949)

### php.ini 配置文件

```ini
[XDebug]
zend_extension=xdebug.so

xdebug.auto_trace=1
xdebug.collect_params=1
xdebug.collect_return=1
xdebug.trace_output_dir ="/tmp/xdebug"
xdebug.profiler_output_dir ="/tmp/xdebug"
xdebug.profiler_output_name = "cachegrind.out.%t.%p"
xdebug.remote_enable = 1
xdebug.remote_autostart = 1
xdebug.remote_handler = "dbgp"
xdebug.remote_host = "127.0.0.1"
# 设置端口号，默认是9000，此处因为本地环境端口冲突故设置为9001（在vscode配置中需要用到）
xdebug.remote_port = 9000
# 这是用于phpstorm中xdebug调试的配置，在vscode中没有用到
xdebug.idekey = phpstorm
```

### VSCode 生成的配置文件

```json
{
  // 使用 IntelliSense 了解相关属性。
  // 悬停以查看现有属性的描述。
  // 欲了解更多信息，请访问: https://go.microsoft.com/fwlink/?linkid=830387
  "version": "0.2.0",
  "configurations": [
    {
      "name": "Listen for XDebug",
      "type": "php",
      "request": "launch",
      "port": 9001
    },
    {
      "name": "Launch currently open script",
      "type": "php",
      "request": "launch",
      "program": "${file}",
      "cwd": "${fileDirname}",
      "port": 9001
    }
  ]
}
```

### VSCode 安装 php debug 插件
