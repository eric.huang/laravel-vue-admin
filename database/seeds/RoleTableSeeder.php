<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RoleTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    DB::table('roles')->insert([
      'name' => 'SUPERVISOR',
      'display_name' => '系统管理员',
      'description' => '系统管理员',
    ]);
    DB::table('roles')->insert([
      'name' => 'MANAGE',
      'display_name' => '管理员',
      'description' => '管理员',
    ]);
    DB::table('roles')->insert([
      'name' => 'EDITOR',
      'display_name' => '编辑',
      'description' => '编辑',
    ]);
    DB::table('roles')->insert([
      'name' => 'USER',
      'display_name' => '用户',
      'description' => '用户',
    ]);
  }
}
