import Vue, { ComponentOptions } from "vue";
import axios from "./util/axios";
import store from "./store/index";
import routerFactoy from './router/index';
import * as _ from 'lodash';
import AppRoot from './app.vue';
import Loading from "./components/Loading.vue";
import ElementUI from 'element-ui';
import SvgIcon from 'vue-svgicon'
import './icons/components'

Vue.use(ElementUI);

declare module 'vue/types/vue' {
    // 3. 声明为 Vue 补充的东西
    interface Vue {
        $loadingBar: Loading;

    }
}

interface vueWindow extends Window {
    axios: any;
    _: any;
    Vue: any
}
declare var window: vueWindow;

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

(function (window: vueWindow, document: any) {
    window.Vue = Vue;
    window._ = _;

    Vue.use(ElementUI);
    Vue.use(SvgIcon, {
        tagName: 'svg-icon',
        defaultWidth: '1em',
        defaultHeight: '1em'
      })


    // const store = storeFactoy(Vuex);
    const router = routerFactoy();
    window.axios = axios(store,router);
    const app = new Vue({
        router,
        store,
        render: (h) => h(AppRoot),
    }).$mount('#app');

})(window, document);


