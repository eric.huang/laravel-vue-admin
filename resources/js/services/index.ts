import axios from 'axios';

export const logoutPost = () =>
  axios.post('/api/logout');

export const loginPost = (user: any) =>
  axios.post('/api/login', user);

export const refresh = () =>
  axios.get('/api/refresh');

export const me = () =>
  axios.post('/api/me');

export const user_list = (page: number) =>
  axios.get('/api/user_list' + (page ? "?page=" + page : ""));

export const user_save = (user: any): Promise<any> =>
  axios.post('/api/user_save', user);

export const user_save_role = (id: number, roles: number[]): Promise<any> =>
  axios.post('/api/user_save_role', { 'userId': id, 'roles': roles });

export const user_remove = (id: number): Promise<any> =>
  axios.post('/api/user_remove/' + id.toLocaleString())

export const user_info = (id: number): Promise<any> =>
  axios.get('/api/user/' + id.toLocaleString());


export const role_save = (role: any): Promise<any> =>
  axios.post('/api/role_save', role);

export const role_list = (page: number): Promise<any> =>
  axios.get('/api/role_list' + (page ? "?page=" + page : ""));

export const role_info = (id: number): Promise<any> =>
  axios.get('/api/role/' + id);

export const role_all = (): Promise<any> =>
  axios.get('/api/role_all')

export const role_save_permission = (id: number, permissions: number[]): Promise<any> =>
  axios.post('/api/role_save_permission', { 'roleId': id, 'permissions': permissions });

export const role_remove = (id: number): Promise<any> =>
  axios.post('/api/role_remove/' + id.toLocaleString())

/**
* 权限操作区域
* @param permission
* @returns {Promise<any>}
*/
export const permission_save = (permission: any): Promise<any> =>
  axios.post('/api/permission_save', permission);

export const permission_list = (page: number): Promise<any> =>
  axios.get('/api/permission_list' + (page ? "?page=" + page : ""));

export const permission_remove = (id: number): Promise<any> =>
  axios.post('/api/permission_remove/' + id.toLocaleString());

export const permission_all = (): Promise<any> =>
  axios.get('/api/permission_all');

export const attachment_list = (page: number): Promise<any> =>
  axios.get('/api/attachment_list' + (page ? "?page=" + page : ""));

export const attachment_remove = (id: number): Promise<any> =>
  axios.post('/api/attachment_remove/' + id.toLocaleString());

export const logs_list = (page: number): Promise<any> =>
  axios.get('/api/logs_list' + (page ? "?page=" + page : ""));

export const backup_list = (page: number): Promise<any> =>
  axios.get('/api/backup_list' + (page ? "?page=" + page : ""));

export const backupUp = (): Promise<any> =>
  axios.post('/api/backup');

export const backup_remove = (name: string): Promise<any> =>
  axios.post('/api/backup_remove/' + name);

