import Cookies from 'js-cookie'
import { VuexModule, Module, Mutation, Action, getModule } from 'vuex-module-decorators'
import store from '../index'
import { Route } from 'vue-router';


export enum DeviceType {
    Mobile,
    Desktop,
}

export interface IAppState {
    _device: DeviceType;
    _sidebar: {
        opened: boolean;
        withoutAnimation: boolean;
    };
    _systemMenu:Route[];

}

@Module({ name: 'app', dynamic: true, store })
export class App extends VuexModule implements IAppState {
    public _sidebar = {
        opened: Cookies.get('sidebarStatus') !== 'closed',
        withoutAnimation: false
    };
    public _device = DeviceType.Desktop;

    public _systemMenu:Route[]=[];

    get sidebar() {
        return this._sidebar;
    }

    get device() {
        return this._device;
    }

    get systemMenu() {
      return this._systemMenu;
    }


    @Action({ commit: "TOGGLE_SIDEBAR" })
    public async ToggleSideBar(withoutAnimation: boolean) {
        return withoutAnimation;
    }

    @Action({ commit: "CLOSE_SIDEBAR" })
    public async CloseSideBar(withoutAnimation: boolean) {
        return withoutAnimation;
    }

    @Action({ commit: "TOGGLE_DEVICE" })
    public async ToggleDevice(device: DeviceType) {
        return device;
    }

    @Mutation
    public SYSTEM_MENU(systemMenu:Route[]) {
        this._systemMenu = systemMenu;
    }

    @Mutation
    private TOGGLE_SIDEBAR(withoutAnimation: boolean) {
        if (this._sidebar.opened) {
            Cookies.set('sidebarStatus', 'closed')
        } else {
            Cookies.set('sidebarStatus', 'opened')
        }
        this._sidebar.opened = !this._sidebar.opened
        this._sidebar.withoutAnimation = withoutAnimation
    }

    @Mutation
    private CLOSE_SIDEBAR(withoutAnimation: boolean) {
        Cookies.set('sidebarStatus', 'closed')
        this._sidebar.opened = false
        this._sidebar.withoutAnimation = withoutAnimation
    }

    @Mutation
    private TOGGLE_DEVICE(device: DeviceType) {
        this._device = device
    }
}

export const AppModule = getModule(App);
