
export function formatSize(size: any) {
  if (null == size || size == '') {
    return "0 Bytes";
  }
  var unitArr = new Array("Bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB");
  var index = 0;
  var srcsize: number = parseFloat(size);
  index = Math.floor(Math.log(srcsize) / Math.log(1024));
  var newSize: number | string = srcsize / Math.pow(1024, index);
  newSize = newSize.toFixed(2);//保留的小数位数
  return newSize + " " + unitArr[index];
}


export function timeShow(timesteam: string) {
  let date :any = new  Date(parseInt(timesteam)*1000);
  return formatTime(date);

}

function formatNumber(n:any):any {
  n = n.toString();
  return n[1] ? n : '0' + n;
}

function formatTime(date:any) {
  var t = getTimeArray(date);
  return [t[0], t[1], t[2]].map(formatNumber).join('-') + ' ' + [t[3], t[4], t[5]].map(formatNumber).join(':')
}

function getTimeArray(date:Date) {
  var year = date.getFullYear();
  var month = date.getMonth() + 1;
  var day = date.getDate();
  var hour = date.getHours();
  var minute = date.getMinutes();
  var second = date.getSeconds();
  return [year, month, day, hour, minute, second].map(formatNumber);
}
