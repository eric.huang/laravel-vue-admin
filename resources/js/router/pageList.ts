import Login from '../views/Login.vue';

import Dashboard from '../views/Dashboard/index.vue';
import Container from "../views/Dashboard/Container.vue"
import Profile from '../views/profile/index.vue';
import System from '../views/profile/system.vue';
import UserInfo from '../views/profile/userinfo.vue';
import Auth from '../views/system/auth.vue';
import Archive from '../views/content/archive.vue';
import Attachment from '../views/content/attachment.vue';
import Log from '../views/system/logs.vue';
import Backup from '../views/system/backup.vue';

export default {
    Login,
    Dashboard,
    Profile,
    System,
    UserInfo,
    Container,
    Auth,
    Archive,
    Attachment,
    Log,
    Backup
} as any;

