export default [{
  "path": "/",
  "name": "首页",
  "componentName": "Dashboard",
  'redirect': '/profile',
  "meta": {
    "title": "首页",
    "hidden": true,
  },
  'children': [{
    'path': '/profile',
    "name": "个人信息",
    "componentName": "Container",
    'redirect': '/profile/userinfo',
    "meta": {
      "title": "个人信息",
      "icon": "link"
    },
    'children': [{
      'path': 'userinfo',
      "name": "用户信息",
      "componentName": "UserInfo",
      "meta": {
        "title": "用户信息",
        "icon": "example"
      },
    }, {
      'path': 'system',
      "name": "系统信息",
      "componentName": "System",
      "meta": {
        "title": "系统信息",
        "icon": "example"
      }
    }]
  },
  {
    'path': '/content',
    "name": "內容管理",
    "componentName": "Container",
    'redirect': '/content/archiver',
    "meta": {
      "title": "內容管理",
      "icon": "link"
    },
    'children': [{
      'path': 'archiver',
      "name": "文章管理",
      "componentName": "Archive",
      "meta": {
        "title": "文章管理",
        "icon": "user"
      }
    }, {
      'path': 'attachment',
      "name": "附件管理",
      "componentName": "Attachment",
      "meta": {
        "title": "附件管理",
        "icon": "example"
      }
    }]
  },
  {
    'path': '/manger',
    "name": "系统管理",
    "componentName": "Container",
    'redirect': '/manger/auth',
    "meta": {
      "title": "系统管理",
      "icon": "link"
    },
    'children': [{
      'path': 'auth',
      "name": "权限管理",
      "componentName": "Auth",
      "meta": {
        "title": "权限管理",
        "icon": "user"
      }
    }, {
      'path': 'backup',
      "name": "备份管理",
      "componentName": "Backup",
      "meta": {
        "title": "备份管理",
        "icon": "example"
      }
    },{
      'path': 'logs',
      "name": "操作日记",
      "componentName": "Log",
      "meta": {
        "title": "操作日记",
        "icon": "example"
      }
    }]
  }]

}];
