<?php

namespace App\Repositories\Contracts;

use App\Repositories\BaseInterface\RepositoryInterface;


/**
 * Created by PhpStorm.
 * User: zxwwjs@hotmail.com
 *
 * @see
 */
interface AttachmentRepositoryInterface extends RepositoryInterface
{

  public function localFileInfo($id);

  public function upload($file);

  public function imageSize($id, $newWidth = 0, $newHeight = 0, $scale = true);
}
