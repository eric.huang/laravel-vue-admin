<?php

namespace App\Repositories;

use App\Repositories\BaseInterface\Repository;
use App\Repositories\Contracts\PermissionRepositoryInterface;


class PermissionRepository extends Repository implements PermissionRepositoryInterface
{

  public function model()
  {
    return config('permission.models.permission');
  }

  public function save($data, $id = 0)
  {
    $id = intval($id);

    if ($id > 0) {
      $data = $this->updateRich($data, $id);
    } else {
      $data = $this->create($data);
    }
    return $data;
  }
}
