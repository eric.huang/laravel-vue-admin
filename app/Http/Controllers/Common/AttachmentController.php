<?php

namespace App\Http\Controllers\Common;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Contracts\AttachmentRepositoryInterface;
use Illuminate\Support\Facades\Log;

class AttachmentController extends Controller
{
    private  $attachmentRepository;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(AttachmentRepositoryInterface $attachmentRepository)
    {
        $this->attachmentRepository = $attachmentRepository;
    }

    public function upload(Request $request)
    {

        $this->attachmentRepository->upload($request->file("avatar"));
        return $this->result(true);
    }
}
