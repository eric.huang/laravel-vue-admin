<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\Contracts\AttachmentRepositoryInterface;

class AttachmentController extends Controller
{

  /**
   * @var \App\Repositories\Contracts\AttachmentRepositoryInterface
   */
  private $attachmentRepository;


  public function __construct(AttachmentRepositoryInterface $attachmentRepository)
  {
    $this->attachmentRepository = $attachmentRepository;
  }

  public function lists()
  {
    $result = $this->attachmentRepository->paginate();
    return $this->result(true, $result);
  }

  public function remove($id = 0)
  {
    $id = intval($id);
    if ($id < 1) {
      return $this->result(false);
    }
    return $this->result($this->attachmentRepository->delete($id));
  }
}
