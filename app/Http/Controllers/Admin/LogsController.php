<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\Contracts\PermissionRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Models\Log;

class LogsController extends Controller
{
  public function __construct()
  { }


  public function lists()
  {
    $result = Log::orderBy('created_at', 'desc')->paginate();
    return $this->result(true, $result);
  }
}
