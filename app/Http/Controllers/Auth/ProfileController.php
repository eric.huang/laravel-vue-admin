<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Contracts\UserRepositoryInterface;
use App\Repositories\Contracts\AttachmentRepositoryInterface;
use Illuminate\Support\Facades\Log;

class ProfileController extends Controller
{
    private $userRepository;
    private $attachmentRepository;

    public function __construct(UserRepositoryInterface $userRepository, AttachmentRepositoryInterface $attachmentRepository)
    {
        $this->userRepository = $userRepository;
        $this->attachmentRepository = $attachmentRepository;
    }


    public function avatar(Request $requst)
    {
        $user = auth()->user();
        if (!$user) {
            return $this->result(false, null, "用户不存在");
        }
        $userId=$user->id;
        $file = $requst->file("file");
        $uploadInfo = $this->attachmentRepository->upload($file);
        if (is_array($uploadInfo) && count($uploadInfo) < 1) {
            return $this->result(false, null, "上传文件失败");
        }
        $attachment_id = $uploadInfo[0];
        return $this->resultData($this->userRepository->avatar($user, $attachment_id));
    }
}
