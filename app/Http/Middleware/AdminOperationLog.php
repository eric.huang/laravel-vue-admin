<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\Log;
use Illuminate\Support\Facades\Auth;

class AdminOperationLog
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user_id = 0;
        if (Auth::check()) {
            $user_id = (int) Auth::id();
        }
        //$_SERVER['admin_uid'] = $user_id;
        if ("GET" != $request->getMethod()) {
            $input = $request->except(['password','pwd']);
            $log = new Log(); # 提前创建表、model
            $log->user_id = $user_id;
            $log->path = $request->path();
            $log->method = $request->method();
            $log->ip = $request->ip();
            $log->sql = '';
            $log->parameter = json_encode($input, JSON_UNESCAPED_UNICODE);
            $log->save();   # 记录日志
        }
        return $next($request);
    }
}
