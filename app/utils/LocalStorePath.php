<?php
namespace App\Utils;

use Illuminate\Support\Facades\Storage;

class LocalStorePath
{
  private $url;

  private $store;

  private $localFilePath;

  private $realFilePath;

  /**
   *
   */
  public function __construct($url, $storeType = "public")
  {
    $this->store = Storage::disk($storeType);
    $this->setUrl($url);
  }

  public function setUrl($url){
    $this->url = $url;
    $this->localFilePath = str_replace($this->store->url("/"), "", $this->url);
    $this->realFilePath =$this->store->path($this->localFilePath);
  }

  public function exists(){
    return $this->store->exists($this->localFilePath);
  }

  public function getUrl()
  {
    return $this->url;
  }

  public function getLocalFilePath()
  {
    return $this->localFilePath;
  }

  public function getFileName()
  {
    return  basename($this->localFilePath);
  }

  public function getLocalPath()
  {
    return  dirname($this->localFilePath);
  }

  public function getExtension()
  {
    $filename = $this->getFileName();
    return substr($filename, strrpos($filename, '.') + 1);
  }

  public function getMainName()
  {
    $filename = $this->getFileName();
    return substr($filename, 0, strrpos($filename, '.'));
  }

  public function getRealPath() {
    return dirname($this->realFilePath);
  }

  public function getRealFilePath() {
    return $this->realFilePath;
  }

  public function thumbnailName($extendName){
    return $this->getMainName()."_".$extendName.".".$this->getExtension();
  }

  public function getThumbnailLocalName($extendName){
    return $this->getLocalPath().DIRECTORY_SEPARATOR.$this->thumbnailName($extendName);
  }

  public function existsThumbnailName($extendName) {
    return $this->store->exists($this->getThumbnailLocalName($extendName));
  }

  public function getThumbnailRealName($extendName) {
    return $this->store->path($this->getThumbnailLocalName($extendName));
  }

  public function getThumbnailUrl($extendName) {
    return $this->store->url($this->getThumbnailLocalName($extendName));
  }

  public function remove() {
    return $this->store->delete($this->localFilePath);
  }

}
