<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Attachment extends Model
{
    public $timestamps = false;

    public $fillable = ['md5', 'filename', 'ext_name', 'size', 'mime_type', 'url'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'md5',
        'mime_type',
        'pivot'
    ];

    public function getInfoAttribute($value)
    {
        if ($value) {
            return json_decode($value, true);
        }
        return null;
    }

    public function setInfoAttribute($value)
    {
        if ($value) {
            $this->attributes['info'] = json_encode($value);
        } else {
            $this->attributes['info'] = null;
        }
    }

    public function owner(){
        return $this->hasMany('App\Models\Attachmentable');
    }
}
