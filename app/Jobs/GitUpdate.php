<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class GitUpdate implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    private $branchName;

    /**
     * Create a new job instance.
     *
     * @param $branchName
     */
    public function __construct($branchName)
    {
        $this->branchName = $branchName;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $dirname = base_path();
        $branchName = $this->branchName; //分支名，只对此分支进行更新
        system("cd $dirname;git fetch --all;git reset --hard origin/$branchName;" +
            "git pull;cd $dirname;pwd;php artisan optimize");
    }
}
