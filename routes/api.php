<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

// ,"middleware"=>['web']
Route::group(['prefix' => "",'middleware' => ['AdminLog']], function () {
    Route::post('login', 'Auth\AuthController@login')->middleware();
    Route::post('upload', 'Common\AttachmentController@upload');
    Route::any('test', 'Common\TestController@test');
});

Route::group(['prefix' => "", 'middleware' => ['auth:api','AdminLog'], "namespace" => 'Auth'], function () {
    Route::post('logout', 'AuthController@logout');
    Route::get('refresh', 'AuthController@refresh'); //->middleware('jwt:refresh');
    Route::post('me', 'AuthController@me');
    Route::post('user_avatar', 'ProfileController@avatar');
});

Route::group(['prefix' => "", 'middleware' => ['auth:api','AdminLog'], 'namespace' => 'Admin'], function () {
    Route::get('user_list', 'UserController@lists')->name('user_list');
    Route::post('user_save', 'UserController@save')->name('user_save');
    Route::post('user_save_role', 'UserController@save_role')->name('user_save_role');
    Route::post('user_remove/{id}', 'UserController@remove')->name('user_save');
    Route::get('user/{id}', 'UserController@info')->name('user_info');


    Route::get('role_all', 'RoleController@all')->name('role_all');
    Route::get('role_list', 'RoleController@lists')->name('role_list');
    Route::post('role_save', 'RoleController@save')->name('role_save');
    Route::get('role/{id}', 'RoleController@info')->name('role_info');
    Route::post('role_remove/{id}', 'RoleController@remove')->name('role_save');
    Route::post('role_save_permission', 'RoleController@save_permission')->name('role_save_permission');

    Route::get('permission_all', 'PermissionController@all')->name('permission_all');
    Route::get('permission_list', 'PermissionController@lists')->name('permission_list');
    Route::post('permission_save', 'PermissionController@save')->name('permission_save');
    Route::post('permission_remove/{id}', 'PermissionController@remove')->name('permission_remove');

    Route::get('attachment_list', 'AttachmentController@lists')->name('attachment_list');
    Route::post('attachment_remove/{id}', 'AttachmentController@remove')->name('attachment_remove');

    Route::get('logs_list', 'LogsController@lists')->name('logs_list');

    Route::get('backup_list', 'BackupController@lists')->name('backup_list');
    Route::post('backup_remove/{name}', 'BackupController@remove')->name('backup_remove');
    Route::post('backup', 'BackupController@backup')->name('backup');

});
