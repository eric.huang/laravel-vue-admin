# laravel-vue-admin

此项目为一个学习laravel ,vue,element-ui构造一个后台管理系统程序。

**2019年5月，用laravel 5.8,Typescript 进行全部重写。**

QQ交流群：201507510

## 近期工作任务

- [ ] 权限管理
- [ ] CMS文章管理
- [x] 对Vue进行Typescript改造

## 主要使用到的技术

- laravel
  - Repository 模式，原定义为对数据访问层的抽象，在本例中来写逻辑业务的内容
  - tymon/jwt-auth 在Vue中的访问都是axios库进行访问
  - laravel-permission 进行权限管理
  - 单元测试，在laravel上做开发，先写测试，还写相关的业务逻辑。
  - 数据库使用lavaval的Eloquent ORM方式
- Vue
  - Vuex 数据流
  - element-ui UI 界面
  - axios AJAX访问与拦截
  - Vue-route 路由
  - Eslint 对js代码进行样式验证

## 参考网站

- [Vue 中文文档](https://www.vuejs.com/)
- [element UI 官网](http://element-cn.eleme.io/)
- [vue element admin](https://github.com/PanJiaChen/vue-element-admin) 一个很好的Vue demo
- [Laravel 学院 Laravel 5.8 中文文档](https://laravelacademy.org/laravel-docs-5_8)
- [Laravel permission 权限管理](https://github.com/spatie/laravel-permission)
