<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\UploadedFile;

class CommonTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testUpload()
    {


        $response = $this->post('/api/upload',[
            'avatar' => UploadedFile::fake()->image('avatar.jpg')
        ]);

        $response->assertStatus(200);
    }

    public function testAvatar(){
        $response = $this->post('/api/user_avatar',[
            'id'=>1,
            'avatar' => UploadedFile::fake()->image('avatar.jpg',100,100)
        ]);

        $response->assertStatus(200);

    }
}
