<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\UploadedFile;

use Tymon\JWTAuth\Facades\JWTAuth;

class BackupTest extends TestCase
{


  /**
   * A basic test example.
   *
   * @return void
   */
  public function testBackupInfo()
  {
    $user = \App\Models\User::find(1);
    $response = $this->be($user,'api')->get('/api/backup_list');
    $response->assertStatus(200);
  }

  public function testBackup()
  {
    $user = \App\Models\User::find(1);
    $response = $this->be($user,'api')->post('/api/backup');
    $response->assertStatus(200);
    var_dump($response->getContent());
  }


}
