<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Utils\LocalStorePath;

class LocalStorePathTest extends TestCase
{

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasic()
    {
        $path = "http://localhost/upload/2019/06-22/20190622-53389623.png";
        $fileInfo = new LocalStorePath($path);

        $this->assertEquals($fileInfo->getExtension(),"png");
        $this->assertEquals($fileInfo->getLocalFilePath(),"2019/06-22/20190622-53389623.png");
        $this->assertEquals($fileInfo->thumbnailName("200x000"),"20190622-53389623_200x000.png");
        $this->assertEquals($fileInfo->getThumbnailLocalName("200x000"),"2019/06-22/20190622-53389623_200x000.png");
        $this->assertEquals($fileInfo->getRealFilePath("200x000"),"/home/jingmin/project/blog/storage/app/public/2019/06-22/20190622-53389623.png");
        $this->assertEquals($fileInfo->getThumbnailLocalName("200x000"),"2019/06-22/20190622-53389623_200x000.png");
        $this->assertEquals($fileInfo->exists(),1);
        $this->assertEquals($fileInfo->getThumbnailRealName("200x000"),"/home/jingmin/project/blog/storage/app/public/2019/06-22/20190622-53389623_200x000.png");

    }
}
